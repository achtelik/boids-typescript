import {Vector2Test} from "./Vector2Test";
import {BoidTest} from "./BoidTest";
import {BoidViewConstraintTest} from "./boid/behavior/BoidViewConstraintTest";

new Vector2Test().test();
new BoidTest().test();
new BoidViewConstraintTest().test()