import {Boid} from "../main/boid/Boid";
import {Vector2} from "../main/Vector2";

declare var QUnit: any;

export class BoidTest {
    test() {
        this.updateTest();
        //this.canSeeTest();
    }

    updateTest() {
        QUnit.test("Boid.update test", function (assert) {
            let boid = new Boid(new Vector2(10, 20), new Vector2(1, 0));
            boid.update(1, new Array<Boid>());

            assert.ok(boid.position.equals(new Vector2(11, 20)), "Moved right!");
        });
    }

    canSeeTest() {
        QUnit.test("Boid.canSee test", function (assert) {
            let boid1 = new Boid(new Vector2(10, 10), new Vector2(0, 1));
            let boid2Visible = new Boid(new Vector2(15, 15), new Vector2(1, 0));
            let boid3InVisible = new Boid(new Vector2(150, 150), new Vector2(1, 0));
            let boid4InVisible = new Boid(new Vector2(10, 5), new Vector2(0, 1));

            assert.ok(boid1.canSee(boid2Visible), "Can see you!");
            assert.ok(!boid1.canSee(boid3InVisible), "Can't see you!");
            assert.ok(!boid1.canSee(boid4InVisible), "Can't see you!");
        });
    }
}