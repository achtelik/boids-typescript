import {Vector2} from "src/main/Vector2";

declare var QUnit: any;

export class Vector2Test {
    test() {
        this.addTest();
        this.subTest();
        this.multiplyTest();
        this.sumTest();
        this.scalarTest();
        this.angleTest();
    }

    addTest() {
        QUnit.test("Vector2.add test", function (assert) {
            let vector1: Vector2 = new Vector2(1, 2).add(new Vector2(3, 4));
            assert.deepEqual(vector1, new Vector2(4, 6), "(1,2) + (3,4) = (4,6)");
        });
    }

    subTest() {
        QUnit.test("Vector2.sub test", function (assert) {
            let vector1: Vector2 = new Vector2(1, 2).sub(new Vector2(3, 5));
            assert.deepEqual(vector1, new Vector2(-2, -3), "(1,2) - (3,5) = (-2,-3)");
        });
    }

    multiplyTest() {
        QUnit.test("Vector2.multiply test", function (assert) {
            let vector1 = new Vector2(1, 2).multiply(3);
            assert.ok(vector1.x == 3 && vector1.y == 6, "(1,2) * 3 = (3,6)");
        });
    }

    divideTest() {
        QUnit.test("Vector2.divide test", function (assert) {
            let vector1 = new Vector2(6, 9).divide(3);
            assert.ok(vector1.x == 2 && vector1.y == 3, "(6,9) / 3 = (2,3)");
        });
    }

    sumTest() {
        QUnit.test("Vector2.sum test", function (assert) {
            let sum = new Vector2(3, 7).sum();
            assert.ok(sum > 7.61577 && sum < 7.61578, "sum = " + sum);
        });
    }

    scalarTest() {
        QUnit.test("Vector2.scalar test", function (assert) {
            let scalar: number = new Vector2(3, 6).scalar(new Vector2(5, 7));
            assert.equal(scalar, 57, "scalar = " + scalar);
        });
    }

    angleTest() {
        QUnit.test("Vector2.angle test", function (assert) {
            let angle = new Vector2(0, 1).angle(new Vector2(0, 1)) * (180 / Math.PI);
            assert.equal(angle, 0, "angle = " + angle);

            angle = new Vector2(1, 1).angle(new Vector2(0, 1)) * (180 / Math.PI);
            assert.ok(angle > 45 && angle < 45.00001, "angle = " + angle);

            angle = new Vector2(1, 2).angle(new Vector2(0, 1)) * (180 / Math.PI);
            assert.ok(angle > 26.5 && angle < 26.566, "angle = " + angle);

            angle = new Vector2(0, -1).angle(new Vector2(0, 1)) * (180 / Math.PI);
            assert.equal(angle, 180, "angle = " + angle);

            angle = new Vector2(-1, -1).angle(new Vector2(0, 1)) * (180 / Math.PI);
            assert.equal(angle, 135, "angle = " + angle);

            angle = new Vector2(1, -2).angle(new Vector2(0, 1)) * (180 / Math.PI);
            assert.ok(angle > 153.434 && angle < 153.435, "angle = " + angle);
        });
    }
}