import {Boid} from "../../../main/boid/Boid";
import {Vector2} from "../../../main/Vector2";
import {BoidViewConstraint} from "../../../main/boid/behavior/BoidViewConstraint";
declare var QUnit: any;

export class BoidViewConstraintTest {

    test() {
        this.checkInViewTest();
        this.checkInDistanceTest();
        this.checkInAngleTest();
    }

    checkInViewTest() {

    }

    checkInDistanceTest() {
        QUnit.test("BoidViewConstraint.checkInDistance test", function (assert) {
            let boidViewConstraint = BoidViewConstraint.getInstance;
            let boid1 = new Boid(new Vector2(10, 10), new Vector2(0, 1));
            let boid2Visible = new Boid(new Vector2(15, 15), new Vector2(1, 0));
            let boid3InVisible = new Boid(new Vector2(150, 150), new Vector2(1, 0));
            let boid4InVisible = new Boid(new Vector2(30, -35), new Vector2(0, 1));

            assert.ok(boidViewConstraint().checkInDistance(boid1, boid2Visible), "Can see you!");
            assert.ok(!boidViewConstraint().checkInDistance(boid1, boid3InVisible), "Can't see you!");
            assert.ok(!boidViewConstraint().checkInDistance(boid1, boid4InVisible), "Can't see you!");
        });
    }

    checkInAngleTest() {
        QUnit.test("BoidViewConstraint.checkInAngle test", function (assert) {
            let boidViewConstraint = BoidViewConstraint.getInstance;
            let boid1 = new Boid(new Vector2(10, 10), new Vector2(1, 0));
            let boid2Visible = new Boid(new Vector2(20, 10), new Vector2(1, 0));
            let boid3InVisible = new Boid(new Vector2(20, 20), new Vector2(1, 0));
            let boid4InVisible = new Boid(new Vector2(-10, 10), new Vector2(0, 1));

            assert.ok(boidViewConstraint().checkInAngle(boid1, boid2Visible), "Can see you!");
            assert.ok(boidViewConstraint().checkInAngle(boid1, boid3InVisible), "Can see you!");
            assert.ok(!boidViewConstraint().checkInAngle(boid1, boid4InVisible), "Can't see you!");
        });
    }
}