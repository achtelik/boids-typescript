import {Swarm} from "./Swarm";
import {GameCanvasLayers} from "./global/GameCanvasLayers";
import {GameConfig} from "./global/GameConfig";
import {Vector2} from "./Vector2";
import {Boid} from "./boid/Boid";

var swarm = new Swarm();

/*swarm.add(new Boid(new Vector2(100, 100), new Vector2(1, 0)));
 swarm.add(new Boid(new Vector2(110, 100), new Vector2(1, 1)));
 swarm.add(new Boid(new Vector2(120, 100), new Vector2(0, 1)));
 swarm.add(new Boid(new Vector2(130, 100), new Vector2(-1, 1)));
 swarm.add(new Boid(new Vector2(140, 100), new Vector2(-1, 0)));
 swarm.add(new Boid(new Vector2(150, 100), new Vector2(-1, -1)));
 swarm.add(new Boid(new Vector2(160, 100), new Vector2(0, -1)));
 swarm.add(new Boid(new Vector2(170, 100), new Vector2(1, -1)));
 */
/*
 swarm.add(new Boid(new Vector2(100, 100), new Vector2(1, 0)));
 swarm.add(new Boid(new Vector2(100, 110), new Vector2(1, 0)));
 swarm.add(new Boid(new Vector2(100, 120), new Vector2(1, 0)));
 swarm.add(new Boid(new Vector2(100, 130), new Vector2(1, 0)));
 swarm.add(new Boid(new Vector2(100, 140), new Vector2(1, 0)));
 */
/*
 swarm.add(new Boid(new Vector2(140, 110), new Vector2(-1, 0)));
 swarm.add(new Boid(new Vector2(150, 120), new Vector2(-1, -1)));
 swarm.add(new Boid(new Vector2(160, 80), new Vector2(0, -1)));
 swarm.add(new Boid(new Vector2(170, 90), new Vector2(1, -1)));
 */

for (var x = 0; x < 100; x++) {
    swarm.add(new Boid(new Vector2(Math.random() * 200, Math.random() * 200), new Vector2(Math.random(), Math.random())));
}

GameCanvasLayers.scale(1);
swarm.render();

window.addEventListener("click", function () {
    GameConfig.GAME_RUNTINE_MOVING = !GameConfig.GAME_RUNTINE_MOVING;
    //tick();
});

/*function tick() {
 swarm.update(0.1);
 GameCanvasLayers.clear();
 swarm.render();
 }*/


var now, dt, last = timestamp();

function timestamp() {
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

function frame() {
    now = timestamp();
    dt = (now - last) / 1000;    // duration in seconds

    swarm.update(dt);

    GameCanvasLayers.clear();
    swarm.render();

    last = now;
    requestAnimationFrame(frame);
}

requestAnimationFrame(frame);
