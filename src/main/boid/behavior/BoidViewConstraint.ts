import {Boid} from "../Boid";
import {GameConfig} from "../../global/GameConfig";

export class BoidViewConstraint {
    private static instance = new BoidViewConstraint();

    private constructor() {
    }

    static getInstance() {
        return BoidViewConstraint.instance;
    }

    checkInView(boid: Boid, otherBoid: Boid): boolean {
        return (this.checkInDistance(boid, otherBoid) &&
        this.checkInAngle(boid, otherBoid));
    }

    checkInDistance(boid: Boid, otherBoid: Boid): boolean {
        let distance = boid.position.sub(otherBoid.position).sum();
        return (distance <= GameConfig.BOID_VIEW_MAXDISTANCE);
    }

    checkInAngle(boid: Boid, otherBoid: Boid): boolean {
        let angle = boid.direction.angle(otherBoid.position.sub(boid.position));
        return (angle <= GameConfig.BOID_VIEW_MAXANGLE);
    }
}