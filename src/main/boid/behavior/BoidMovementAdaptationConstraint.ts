import {Boid} from "../Boid";
import {Vector2} from "../../Vector2";
import {GameConfig} from "../../global/GameConfig";
export class BoidMovementAdaptationConstraint {
    private globalDirection: Vector2;
    private globalCount: number;
    private boid: Boid;

    constructor(boid: Boid) {
        this.boid = boid;
        this.init();
    }

    addInfluencer(otherBoid: Boid) {
        this.globalDirection = this.globalDirection.add(otherBoid.direction);
        this.globalCount++;
    }

    determineInfluence(): Vector2 {
        if (this.globalCount === 0) {
            return new Vector2(0, 0);
        }
        let result = this.globalDirection.divide(this.globalCount).sub(this.boid.direction).multiply(GameConfig.BOID_BEHAVIOR_ADAPTION_FACTOR);
        this.init();
        return result;
    }

    private init() {
        this.globalDirection = new Vector2(0, 0);
        this.globalCount = 0;
    }
}