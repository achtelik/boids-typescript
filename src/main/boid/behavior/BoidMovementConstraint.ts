import {Boid} from "../Boid";
import {GameConfig} from "../../global/GameConfig";
import {Vector2} from "../../Vector2";
import {BoidMovementAdaptationConstraint} from "./BoidMovementAdaptationConstraint";
import {BoidMovementSeparationConstraint} from "./BoidMovementSeparationConstraint";
import {BoidMovementCohesionConstraint} from "./BoidMovementCohesionConstraint";
export class BoidMovementConstraint {
    private separationConstraint: BoidMovementSeparationConstraint;
    private cohesionConstraint: BoidMovementCohesionConstraint;
    private adaptationConstraint: BoidMovementAdaptationConstraint;

    private boid: Boid;

    constructor(boid: Boid) {
        this.boid = boid;
        this.separationConstraint = new BoidMovementSeparationConstraint(boid);
        this.cohesionConstraint = new BoidMovementCohesionConstraint(boid);
        this.adaptationConstraint = new BoidMovementAdaptationConstraint(boid);
    }

    determineDirection(boids: Boid[]): Vector2 {
        for (let otherBoid of boids) {
            if (!this.boid.equals(otherBoid) && this.boid.canSee(otherBoid)) {
                this.separationConstraint.addInfluencer(otherBoid);
                this.cohesionConstraint.addInfluencer(otherBoid);
                this.adaptationConstraint.addInfluencer(otherBoid);
            }
        }
        let separationCorrection = this.separationConstraint.determineInfluence();
        let cohesionCorrection = this.cohesionConstraint.determineInfluence();
        let adaptionCorrection = this.adaptationConstraint.determineInfluence();
        let correction = separationCorrection.add(cohesionCorrection).add(adaptionCorrection);
        return this.restrictDirection(this.boid.direction.add(correction));
    }


    restrictDirection(direction: Vector2) {
        let tmpDirectionChange;
        if (direction.sum() > GameConfig.BOID_BEHAVIOR_MAXSPEED) {
            let tmpDirectionSpeedScalar = GameConfig.BOID_BEHAVIOR_MAXSPEED / direction.sum();
            tmpDirectionChange = direction.multiply(tmpDirectionSpeedScalar);
        } else {
            tmpDirectionChange = direction;
        }
        return tmpDirectionChange;
    }
}