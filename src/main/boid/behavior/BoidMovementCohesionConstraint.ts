import {Vector2} from "../../Vector2";
import {Boid} from "../Boid";
import {GameConfig} from "../../global/GameConfig";
export class BoidMovementCohesionConstraint {
    private globalPosition: Vector2;
    private globalCount: number;
    private boid: Boid;

    constructor(boid: Boid) {
        this.boid = boid;
        this.init();
    }

    addInfluencer(otherBoid: Boid) {
        this.globalPosition = this.globalPosition.add(otherBoid.position);
        this.globalCount++;
    }

    determineInfluence(): Vector2 {
        if (this.globalCount === 0) {
            return new Vector2(0, 0);
        }
        let result = this.globalPosition.divide(this.globalCount).sub(this.boid.position).multiply(GameConfig.BOID_BEHAVIOR_COHESION_FACTOR);
        this.init();
        return result;
    }

    private init() {
        this.globalPosition = new Vector2(0, 0);
        this.globalCount = 0;
    }
}