import {Boid} from "../Boid";
import {Vector2} from "../../Vector2";
import {GameConfig} from "../../global/GameConfig";
export class BoidMovementSeparationConstraint {
    private globalPosition: Vector2;
    private boid: Boid;

    constructor(boid: Boid) {
        this.boid = boid;
        this.init();
    }

    addInfluencer(otherBoid: Boid) {
        let distance = this.boid.position.sub(otherBoid.position).sum();
        if (distance < GameConfig.BOID_BEHAVIOR_SEPARATION_MINDISTANCE) {
            this.globalPosition = this.globalPosition.add(this.boid.position.sub(otherBoid.position).multiply(GameConfig.BOID_BEHAVIOR_SEPARATION_FACTOR))
        }
    }

    determineInfluence(): Vector2 {
        let result = this.globalPosition;
        this.init();
        return result
    }

    private init() {
        this.globalPosition = new Vector2(0, 0);
    }
}