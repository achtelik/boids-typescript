import {Vector2} from "../Vector2";
import {BoidRenderer} from "./BoidRenderer";
import {BoidViewConstraint} from "./behavior/BoidViewConstraint";
import {BoidMovementConstraint} from "./behavior/BoidMovementConstraint";
import {GameConfig} from "../global/GameConfig";
import {UnicIdCounter} from "../util/UnicIdCounter";
import {GameCanvasLayers} from "../global/GameCanvasLayers";

export class Boid {
    readonly id: number = UnicIdCounter.getInstance().generateId();
    position: Vector2;
    direction: Vector2;

    private boidRenderer: BoidRenderer = BoidRenderer.getInstance();
    private boidMovementConstraing: BoidMovementConstraint;
    private boidViewConstraint: BoidViewConstraint = BoidViewConstraint.getInstance();

    constructor(position: Vector2, direction: Vector2) {
        this.position = position;
        this.direction = direction;

        this.boidMovementConstraing = new BoidMovementConstraint(this);
    }

    update(timeDelta: number, boids: Boid[]) {
        if (GameConfig.GAME_RUNTINE_MOVING) {
            this.direction = this.boidMovementConstraing.determineDirection(boids);
            this.position = this.position.add(this.direction.multiply(timeDelta * GameConfig.BOID_MOVEMENT_FACTOR));

            this.position = new Vector2(this.position.x % GameCanvasLayers.worldLayerCanvas.width,
                this.position.y % GameCanvasLayers.worldLayerCanvas.height);
        }
    }

    canSee(otherBoid: Boid): boolean {
        return this.boidViewConstraint.checkInView(this, otherBoid);
    }

    draw() {
        this.boidRenderer.draw(this.position, this.direction);
    }

    equals(otherBoid: Boid): boolean {
        return (this.id === otherBoid.id);
    }
}