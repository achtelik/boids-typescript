import {Vector2} from "../Vector2";
import {GameConfig} from "../global/GameConfig";
import {GameCanvasLayers} from "../global/GameCanvasLayers";

export class BoidRenderer {
    private static instance = new BoidRenderer();

    private constructor() {
    }

    static getInstance() {
        return BoidRenderer.instance;
    }

    draw(position: Vector2, direction: Vector2) {
        this.drawViewArea(position, direction);
        this.drawViewDirection(position, direction);
        this.drawBoid(position, direction);
    }

    drawBoid(position: Vector2, direction: Vector2) {
        let context = GameCanvasLayers.worldLayerContext;
        context.save();
        context.translate(position.x, position.y);
        context.rotate(this.calculateDrawRotation(direction));

        context.beginPath();
        context.moveTo(0, 0);
        context.lineTo(-GameConfig.BOID_DRAW_HEIGHT / 2,
            -GameConfig.BOID_DRAW_WIDTH / 2);
        context.lineTo(GameConfig.BOID_DRAW_HEIGHT / 2, 0);
        context.lineTo(-GameConfig.BOID_DRAW_HEIGHT / 2, GameConfig.BOID_DRAW_WIDTH / 2);
        context.closePath();

        // the outline
        context.lineWidth = GameConfig.BOID_DRAW_LINESIZE;
        context.strokeStyle = GameConfig.BOID_DRAW_COLOR;
        context.stroke();

        // the fill color
        context.fillStyle = GameConfig.BOID_DRAW_COLOR;
        context.fill();

        context.restore();
    }

    drawViewArea(position: Vector2, direction: Vector2) {
        if (GameConfig.BOID_DRAW_VIEWARE) {
            let context = GameCanvasLayers.debugLayerContext;
            context.save();
            context.translate(position.x, position.y);
            context.rotate(this.calculateDrawRotation(direction));

            context.beginPath();
            context.moveTo(0, 0);
            context.arc(0, 0, GameConfig.BOID_VIEW_MAXDISTANCE,
                -GameConfig.BOID_VIEW_MAXANGLE,
                GameConfig.BOID_VIEW_MAXANGLE);
            context.closePath();

            // the fill color
            context.fillStyle = GameConfig.BOID_DRAW_VIEWAREA_COLOR;
            context.fill();
            context.restore();
        }
    }

    drawViewDirection(position: Vector2, direction: Vector2) {
        if (GameConfig.BOID_DRAW_DIRECTION) {
            let context = GameCanvasLayers.debugLayerContext;
            context.save();
            context.translate(position.x, position.y);
            context.rotate(this.calculateDrawRotation(direction));

            context.beginPath();
            context.moveTo(0, 0);
            context.lineTo(GameConfig.BOID_BEHAVIOR_MAXSPEED * GameConfig.BOID_DRAW_DIRETION_FACTOR, 0);
            context.closePath();
            context.lineWidth = GameConfig.BOID_DRAW_DIRECTION_LINESIZE;
            context.strokeStyle = GameConfig.BOID_DRAW_DIRECTION_MAXCOLOR;
            context.stroke();

            context.beginPath();
            context.moveTo(0, 0);
            context.lineTo(direction.sum() * GameConfig.BOID_DRAW_DIRETION_FACTOR, 0);
            context.closePath();
            context.lineWidth = GameConfig.BOID_DRAW_DIRECTION_LINESIZE;
            context.strokeStyle = GameConfig.BOID_DRAW_DIRECTION_CURRENTCOLOR;
            context.stroke();

            context.restore();
        }
    }

    calculateDrawRotation(direction: Vector2) {
        let rotation = new Vector2(1, 0).angle(direction);
        if (direction.y < 0) {
            return 2 * Math.PI - rotation;
        } else {
            return rotation;
        }
    }
}