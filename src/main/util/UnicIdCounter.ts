export class UnicIdCounter {
    private static instance = new UnicIdCounter();
    private id: number = 0;

    private constructor() {
    }

    static getInstance() {
        return UnicIdCounter.instance;
    }

    generateId() {
        return this.id++;
    }
}