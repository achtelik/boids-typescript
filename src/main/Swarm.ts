import {Boid} from "./boid/Boid";

export class Swarm {
    boids: Boid[] = [];

    add(boid: Boid): this {
        this.boids.push(boid);
        return this;
    }

    update(timeDelta: number) {
        for (let boid of this.boids) {
            boid.update(timeDelta, this.boids);
        }
    }

    render() {
        for (let boid of this.boids) {
            boid.draw();
        }
    }
}