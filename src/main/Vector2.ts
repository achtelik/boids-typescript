export class Vector2 {
    readonly x: number;
    readonly y: number;

    constructor(x: number, y: number) {
        this.x = (isNaN(x) ? 0 : x);
        this.y = (isNaN(y) ? 0 : y);
    }

    static random(): Vector2 {
        return new Vector2(Math.random(), Math.random());
    }

    add(otherVector: Vector2): Vector2 {
        let x = this.x + otherVector.x;
        let y = this.y + otherVector.y;
        return new Vector2(x, y);
    }

    sub(otherVector: Vector2): Vector2 {
        let x = this.x - otherVector.x;
        let y = this.y - otherVector.y;
        return new Vector2(x, y);
    }

    multiply(scalar: number): Vector2 {
        let x = this.x * scalar;
        let y = this.y * scalar;
        return new Vector2(x, y);
    }

    divide(divisor: number): Vector2 {
        let x = this.x / divisor;
        let y = this.y / divisor;
        return new Vector2(x, y);
    }

    sum(): number {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    scalar(otherVector: Vector2): number {
        return this.x * otherVector.x + this.y * otherVector.y;
    }

    angle(otherVector: Vector2): number {
        return Math.acos(this.scalar(otherVector) / (this.sum() * otherVector.sum())); //* (180 / Math.PI);
    }

    equals(otherVector2: Vector2): boolean {
        if (this.x === otherVector2.x || this.y === otherVector2.y) {
            return true;
        }
        return false;
    }
}