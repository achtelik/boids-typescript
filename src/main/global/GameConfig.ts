export class GameConfig {
    static BOID_VIEW_MAXDISTANCE: number = 25;
    static BOID_VIEW_MAXANGLE: number = (3 / 4) * Math.PI;

    static BOID_BEHAVIOR_SEPARATION_MINDISTANCE: number = 4;
    static BOID_BEHAVIOR_SEPARATION_FACTOR: number = 1 / 5;
    static BOID_BEHAVIOR_COHESION_FACTOR: number = 1 / 10;
    static BOID_BEHAVIOR_ADAPTION_FACTOR: number = 1 / 8;
    static BOID_BEHAVIOR_MAXSPEED: number = 3 / 2;

    static BOID_MOVEMENT_FACTOR: number = 10;

    static BOID_DRAW_WIDTH: number = 2;
    static BOID_DRAW_HEIGHT: number = 2;
    static BOID_DRAW_LINESIZE: number = 1;
    static BOID_DRAW_COLOR: string = "rgba(37, 182, 255, 1)";

    static BOID_DRAW_VIEWARE: boolean = true;
    static BOID_DRAW_VIEWAREA_COLOR: string = "rgba(39, 55, 64, 0.05)";


    static BOID_DRAW_DIRECTION: boolean = true;
    static BOID_DRAW_DIRECTION_LINESIZE: number = 1;
    static BOID_DRAW_DIRETION_FACTOR: number = 10;
    static BOID_DRAW_DIRECTION_MAXCOLOR: string = "rgba(255, 255, 255, 1)";
    static BOID_DRAW_DIRECTION_CURRENTCOLOR: string = "rgba(174, 0, 255, 1)";

    static GAME_RUNTINE_MOVING: boolean = true;
}