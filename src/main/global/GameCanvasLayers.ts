export class GameCanvasLayers {
    static debugLayerCanvas = <HTMLCanvasElement>(document.getElementById("debug_layer"));
    static debugLayerContext = <CanvasRenderingContext2D>(GameCanvasLayers.debugLayerCanvas.getContext("2d"));
    static worldLayerCanvas = <HTMLCanvasElement>(document.getElementById("world_layer"));
    static worldLayerContext = <CanvasRenderingContext2D>(GameCanvasLayers.worldLayerCanvas.getContext("2d"));

    static scale(scale: number) {
        this.debugLayerContext.scale(scale, scale);
        this.worldLayerContext.scale(scale, scale);
    }

    static clear() {
        this.debugLayerContext.clearRect(0, 0, this.debugLayerCanvas.width, this.debugLayerCanvas.height)
        this.worldLayerContext.clearRect(0, 0, this.worldLayerCanvas.width, this.worldLayerCanvas.height)
    }
}